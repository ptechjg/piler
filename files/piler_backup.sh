#!/bin/bash

DATE=`date -I`
DEST=/var/piler/backup/backup_${DATE}
mkdir -p $DEST

mysqldump --all-databases --skip-lock-tables -h $MYSQL_HOSTNAME  -u root -p"$MYSQL_ROOT_PASSWORD" > ${DEST}/piler.sql

tar cfz ${DEST}/piler_var.tar.gz /var/piler/error  /var/piler/imap /var/piler/sphinx /var/piler/stat /var/piler/store /var/piler/www
tar cfz ${DEST}/piler_etc.tar.gz /etc/piler
