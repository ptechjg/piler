<?php

$config['SITE_NAME'] = 'HOSTNAME';
$config['SITE_URL'] = 'http://' . $config['SITE_NAME'] . '/';

$config['SMTP_DOMAIN'] = $config['SITE_NAME'];
$config['SMTP_FROMADDR'] = 'no-reply@' . $config['SITE_NAME'];
$config['ADMIN_EMAIL'] = 'admin@' . $config['SITE_NAME'];

$config['DB_PASSWORD'] = 'MYSQL_PASSWORD';

$config['PILER_HOST'] = $config['SITE_NAME'];

$config['PILER_BINARY'] = "/usr/sbin/piler";
$config['COMPATIBILITY'] = '';

$config['BRANDING_TEXT'] = '';
$config['BRANDING_URL'] = '';
$config['BRANDING_LOGO'] = '';
$config['BRANDING_BACKGROUND_COLOUR'] = '';
$config['BRANDING_TEXT_COLOUR'] = '';
$config['BRANDING_FAVICON'] = '/view/theme/default/assets/ico/favicon.png';
$config['SUPPORT_LINK'] = '';
$config['TITLE_PREFIX'] = '';

$config['DATE_TEMPLATE'] = 'd.m.Y'; //'Y.m.d';

