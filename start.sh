#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

#set -x

script_path=$(readlink -f "$0")
basedir=${script_path%/*}
script_name=${script_path##*/}

echo "container start.sh"
echo "script_path: $script_path  basedir: $basedir  script_name: $script_name"

DATAROOTDIR="/usr/share"
SYSCONFDIR="/etc"
SPHINXCFG="/etc/piler/sphinx.conf"
FETCHMAIL_CFG="/etc/piler/fetchmailrc"
PILER_HOST=${PILER_HOST:-archive.yourdomain.com}
PILER_CONF="/etc/piler/piler.conf"
PILER_PEM="/etc/piler/piler.pem"
PILER_KEY="/etc/piler/piler.key"
PILER_CERT="/etc/piler/piler.crt"
CONFIG_SITE_PHP="/etc/piler/config-site.php"
PILER_MYSQL_CNF="/etc/piler/.my.cnf"
#SSL_CERT_DATA="/C=US/ST=Denial/L=Springfield/O=Dis/CN=${PILER_HOSTNAME}"
SSL_CERT_DATA="/C=DE/ST=NRW/L=Ahlen/O=Piler/CN=${PILER_HOSTNAME}"


pid=1

# SIGTERM-handler
term_handler() {
  if [ -n "$pid" ] && [ $pid -ne 0 ]; then
    echo "stopping services" 
    service fetchmail stop
    service cron stop
    /etc/init.d/rc.searchd stop
    /etc/init.d/rc.piler stop
    echo "all services stopped, now exiting ..." 
  fi
  #exit 143; # 128 + 15 -- SIGTERM
  exit 0
}


update_config_files() {
   echo "container start.sh: update_config_files()"

   if [[ ! -f "/etc/piler/piler.key" ]]; then
      # copy piler configuration to volume
      cp -a /etc/piler.sik/* /etc/piler
   fi

   echo "Creating my.cnf configuration"
   printf "[mysql]\nhost = ${MYSQL_HOSTNAME}\nuser = ${MYSQL_USERNAME}\npassword = ${MYSQL_PASSWORD}\n\n[mysqldump]\nhost = ${MYSQL_HOSTNAME}\nuser = ${MYSQL_USERNAME}\npassword = ${MYSQL_PASSWORD}\n" > "$PILER_MYSQL_CNF"
   chown piler:piler "$PILER_MYSQL_CNF"
   chmod 400 "$PILER_MYSQL_CNF"

   echo "Creating sphinx configuration"
   sed -e "s%MYSQL_HOSTNAME%$MYSQL_HOSTNAME%" -e "s%MYSQL_DATABASE%$MYSQL_DATABASE%" -e "s%MYSQL_USERNAME%$MYSQL_USERNAME%" -e "s%MYSQL_PASSWORD%$MYSQL_PASSWORD%" $SYSCONFDIR/piler/sphinx.conf.dist > $SPHINXCFG
   echo "Done."

   # MAILSERVER : mailserverof.provider.com
   # MAILPROTO : POP3, IMAP
   # SERVERPORT : POP3: 110,995 IMAP: 143,993
   # MAILDOMAINS : yourmaildomain.com (multiple domains are space-separated)
   # USERNAME : catchallusernam@yourdomain.com
   # USERPASS 
   if [[ ! -f "$FETCHMAIL_CFG" ]]; then
     echo "Updating fetchmail configuration"
     sed -e "s%MAILSERVER%$MAIL_SERVER%" -e "s%MAILPROTO%$MAIL_PROTO%" -e "s%SERVERPORT%$MAIL_PORT%" -e "s%MAILDOMAINS%$MAIL_DOMAINS%" -e "s%USERNAME%$MAIL_USER%" -e "s%USERPASS%$MAIL_PASSWORD%" $SYSCONFDIR/piler/fetchmailrc.dist > $FETCHMAIL_CFG
     sed -i "/# fetchall keep/ a    ${FETCHMAIL_OPT}" $FETCHMAIL_CFG 
     chmod 0600 $FETCHMAIL_CFG
     chown fetchmail $FETCHMAIL_CFG
     ln -s $FETCHMAIL_CFG /etc/fetchmailrc
     echo "Done."
   else
     echo "fetchmail configuration already exists"
     if [[ ! -L "/etc/fetchmailrc" ]]; then
       ln -s $FETCHMAIL_CFG /etc/fetchmailrc
     fi
     chmod 0600 $FETCHMAIL_CFG
     chown fetchmail $FETCHMAIL_CFG
   fi

   if [[ ! -f "$PILER_CONF" ]]; then
      pilerconf | grep -v mysqlsocket | \
      sed -e "s/tls_enable=0/tls_enable=1/g" \
          -e "s/hostid=mailarchiver/hostid=${PILER_HOSTNAME}/g" \
          -e "s/mysqlport=0/mysqlport=3306/" \
          -e "s/mysqlpwd=/mysqlpwd=${MYSQL_PASSWORD}/" \
          -e "s/mysqlhost=/mysqlhost=${MYSQL_HOSTNAME}/" \
          -e "s/mysqluser=piler/mysqluser=${MYSQL_USERNAME}/" \
          -e "s/mysqldb=piler/mysqldb=${MYSQL_DATABASE}/" \
          -e "s/default_retention_days=3652/default_retention_days=${RETENTION_DAYS}/" \
          -e "s/pemfile=/pemfile=\/etc\/piler\/piler.pem/" > "$PILER_CONF"


      chmod 600 "$PILER_CONF"
      chown $PILER_USER:$PILER_USER /etc/piler/piler.conf
   fi

   echo "Updating $CONFIG_SITE_PHP configuration"
   sed -i -e "s/MYSQL_PASSWORD/${MYSQL_PASSWORD}/" "$CONFIG_SITE_PHP"
   sed -i -e "s/HOSTNAME/${PILER_HOSTNAME}/" "$CONFIG_SITE_PHP"
   printf "\$config['SPHINX_HOSTNAME'] = \$config['SITE_NAME'] . ':9306';\n\n\$config['DB_DATABASE'] = '${MYSQL_DATABASE}';\n\$config['DB_PASSWORD'] = '${MYSQL_PASSWORD}';\n\$config['DB_HOSTNAME'] = '${MYSQL_HOSTNAME}';\n\$config['DB_USERNAME'] = '${MYSQL_USERNAME}';\n" >> "$CONFIG_SITE_PHP"


   make_certificate

}

make_certificate() {
   echo "container start.sh: make_certificates()"
   if [[ ! -f "$PILER_PEM" ]]; then
      echo -n "Making an ssl key ... "

      KEYTMPFILE="piler.key"
      KEYFILE="/etc/piler/piler.rnd"

      dd if=/dev/urandom bs=56 count=1 of="$KEYTMPFILE" 2>/dev/null
      if [ $(stat -c '%s' "$KEYTMPFILE") -ne 56 ]; then echo "could not read 56 bytes from /dev/urandom to ${KEYTMPFILE}"; exit 1; fi

      echo -n "installing keyfile (${KEYTMPFILE}) to ${KEYFILE}... "
      cp "$KEYTMPFILE" "$KEYFILE"
      chgrp "$PILER_USER" "$KEYFILE"
      chmod 640 "$KEYFILE"
      rm -f "$KEYTMPFILE"
      echo "Done."

      echo -n "Making an ssl certificate ... "
      openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "$SSL_CERT_DATA" -keyout $PILER_KEY -out $PILER_CERT -sha1
      cat $PILER_KEY > "$PILER_PEM"
      cat $PILER_CERT >> "$PILER_PEM"
      chmod 640 $PILER_PEM $PILER_KEY $PILER_CERT
      chgrp $PILER_USER $PILER_PEM $PILER_KEY $PILER_CERT
      echo "Done."
   fi
}


initialize_piler_data() {
   echo "container start.sh: Initilizing_piler_data()"

   wait-for ${MYSQL_HOSTNAME}:3306 -- echo "mariadb is up"
   sleep 5

   if [[ "$(echo "show tables" | mysql --defaults-file="$PILER_MYSQL_CNF" "$MYSQL_DATABASE")" == "" ]]; then
      # set >> /etc/piler/init_piler_data.log
      if $(echo "create database if not exists $MYSQL_DATABASE character set utf8mb4" | mysql -h $MYSQL_HOSTNAME -u root -p"$MYSQL_ROOT_PASSWORD") ; then
        echo "db created ok" 
      else
        echo "db created with error: $?" 
        exit 127
      fi
      mysql -h $MYSQL_HOSTNAME -u root -p"$MYSQL_ROOT_PASSWORD"  -e "grant all privileges on ${MYSQL_DATABASE}.* to '${MYSQL_USERNAME}' identified by '${MYSQL_PASSWORD}'; flush privileges;"
      mysql --defaults-file="$PILER_MYSQL_CNF" "$MYSQL_DATABASE" < /usr/share/piler/db-mysql.sql
   fi

   if [[ ! -d "/var/piler/error" ]]; then
      # copy piler var structure to volume
      cp -a /var/piler.sik/* /var/piler
   fi

   [[ -f /var/piler/sphinx/main1.spp ]] || su $PILER_USER -c "indexer --all --config ${SPHINXCFG}"

}


start_piler_services() {
   echo "container start.sh: start_piler_services()"

   service rsyslog start

   #CMD ["php-fpm7.0"]
   echo "  starting php7.0-fpm"
   service php7.0-fpm start



   echo "  starting searchd"
   /etc/init.d/rc.searchd start

   echo "  starting piler"
   /usr/sbin/piler -d

   echo "  starting piler-smtp"
   /usr/sbin/piler-smtp -d
   sleep 5

   echo "  starting fetchmail"
   service fetchmail start

   echo "  starting cron"
   service cron start


}

trap 'term_handler' SIGTERM

update_config_files
initialize_piler_data
start_piler_services

echo "starting endless loop"

# run 'tail' in background, then wait for it to finish in an interruptable way (=wait)
while true
do
  tail -f /dev/null & wait ${!}
done
