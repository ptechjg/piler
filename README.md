## Welcome to Piler

Piler is a feature rich open source email archiving solution, and a viable alternative to commercial email 
archiving products.

Piler has a nice GUI written in PHP supporting several authentication methods (AD/LDAP, SSO, Google OAuth, 
2 FA, IMAP, POP3).


Piler supports

* archiving and retention rules
* legal hold
* deduplication
* digital fingerprinting and verification
* full text search
* tagging emails
* view, export, restore emails
* bulk import/export messages
* audit logs
* Google Apps
* Office 365


To learn more about Piler please visit the maintainer's page at 
[www.mailpiler.org](http://www.mailpiler.org/wiki/start) for the community version, 
or at [www.mailpiler.com](http://www.mailpiler.com) for the commercial one.

This docker application uses the community version.

---

## Usage

A complete Piler application consists out of a piler docker image, a nginx image for the GUI and a MariabDB SQL server image. To ease the configuration of these docker containers we will use docker-compose.

To deploy a new application, first create a directory on your docker host and then get the prepared docker compose file: [docker-compose.yml](https://bitbucket.org/ptechjg/piler/src/master/docker-compose.yml)


So let's say you have created a directory '/volume1/docker/piler' on your docker host (e.g. your NAS) where to store all your database, configuration and email files used by Piler..

Within this '/volume1/docker/piler' directory create four sub-directories to gain this structure:


      /volume1/docker/piler/mysql
      /volume1/docker/piler/nginx
      /volume1/docker/piler/etc
      /volume1/docker/piler/var


These directories will contain:

* .../piler/mysql : database files for mariadb
* .../piler/nginx : nginx log files
* .../piler/etc   : piler's configuration directory
* .../piler/var   : piler's file storage and web files directory


Place the docker-compose.yml file you just downloaded into the Piler folder '/volume1/docker/piler' I assume you have created on the Docker host.

Now edit the docker-compose.yml to reflect your personal needs.

The environment variables and the volumes section are the most relevant. Please check all entries in this file and replace their values with your own specific values.

E.g. in all volume sections you have to replace on the left side

```bash
/host-data/mysql    with  /volume1/docker/piler/mysql
/host-data/pil/var  with  /volume1/docker/piler/var
/host-data/pil/etc  with  /volume1/docker/piler/etc
/host-data/nginx    with  /volume1/docker/piler/nginx
```

Be sure to replace all occurences of these entries in the docker-compose.yml.

Furthermore you have to set the appropriate credentials to create and access your new piler databse.

Find and set these mariadb environment definitions:

* MYSQL_ROOT_PASSWORD=[this is the root password of your mariadb instance]
* MYSQL_DATABASE=[the name of the database which will be created in mariadb]
* MYSQL_USERNAME=[a new user who will get access to the piler database only]
* MYSQL_PASSWORD=[the password for the above username]

If you want your new piler application to collect email from a catch-all mail account, you have to set these environment definitions to access the mail account:

* MAIL_SERVER=[FQDN of your provider's imap or pop3 mail server]
* MAIL_PROTO=[use IMAP or POP3]
* MAIL_PORT=[specify the port; common value for imap is 143 or 993, pop3 is 110 or 995]
* MAIL_DOMAINS=[space separated list of domain names you get mail for]
* MAIL_USER=[the user name of your mail account]
* MAIL_PASSWORD=[the password to access your mail account]
* FETCHMAIL_OPT=[options you may want to give the fetchmail service, like 'keep' or 'fetchall']

And at least you have to set the hostname used to access the Web GUI and to send emails to:

* PILER_HOSTNAME=[FQDN of your docker host as used in a browser and as the email target]

By default the nginx web server of your new Piler application listens on the standard web ports 80 (for 'http://' access) and 443 (for 'https://' access).

If you want your docker host to listen on other ports (e.g. 8080 instead of 80) you have to edit this line in the 'nginx' section of your docker-compose.yml file

old:
```bash
ports:
      - "80:80"
      - "443:443"
```

new:
```bash
ports:
      - "8080:80"
      - "8443:443"
```


Every time piler archives a message, it assigns a retention period according to the retention rules or applying the default value (3652 days = 10 years + 2 days). After the retention period has expired the piler utilities will remove the message from the archive.

To set the default value to a more suiteable one you can set

* RETENTION_DAYS=3652


## Starting the application

After you have edited the docker-compose.yml file you are ready to start the application (sudo is neccessary on a Synology NAS):

```bash
sudo docker-compose up -d
```


If you want to check if all three containers are running, issue a 

```bash
sudo docker-compose ps
```




## How to use the Piler application

Once the docker containers are started you can access Piler via its Web-GUI at port 8080 of your docker host, e.g. *http://192.168.x.x:8080* .

You can login with the predefinded administrative account:

```bash
User: admin@local
 Password: pilerrocks
```


## How to archive emails

To archive emails, piler must receive them somehow. So you have to configure your mail server to send a copy of each received emails to piler via smtp.

If you have MS Exchange, then turn on journaling.

If you have postfix (including zimbra), then add the following to main.cf: 

```bash
always_bcc = archive@piler.yourdomain.com
```

If you don't use your own mail server but just have access to the mail service of your web hoster, you will create a new mail account (e.g. archive@yourdomain.com) as a catch-all-account.
This account will get all emails send to the defined email users at your web hoster, and Piler will fetch these emails to archive them.

Furthermore you will configure the email clients of your users to always send a copy of every email your users are transmitting to the catch-all-account, so Piler is able to archive both send and received mails.

As noted above you have already set the credentials of this catch-all-account in your docker-compose.yml file.

Now log in to the Piler GUI and go to 'Administration / Domains' to define all the domains your Piler is receiving mails for.


## Review the archived emails

As soon as your Piler is configured to receive emails, you (as admin@local) are able to check the actual state on the 'health' page ('Monitor / health').

Piler allows you to access only the emails you either sent or received. So the administrator can see only his own emails (and anything their membership allows). 

Normally, you would create users within Piler for each email address you are archiving for, so that those users are able to review their own mails.

If you need a user who can access anyone's emails then grant him AUDITOR privileges on the user settings page.



## Updating the docker image

```bash
sudo docker-compose down
sudo docker pull ptechjg/piler
sudo docker-compose up -d
```


